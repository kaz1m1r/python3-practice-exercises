import kivy
# kivy.require('1.11.1')
from kivy.app import App
from kivy.uix.label import Label

class MyApp(App):
    def build(self):
        return Label(text = "IF YOU WANT SOMETHING YOU SHOULD NEVER STOP TRYING TO OBTAIN IT. YOU HAVE TO WANT IT! \n\n- every successfull programmer ever")

if __name__ == "__main__":
    MyApp().run()
