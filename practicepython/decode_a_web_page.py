import requests
from bs4 import BeautifulSoup

url = "https://www.nytimes.com/"

r = requests.get(url)

print(r.text)

s = requests.get(url)

print(r.headers) # r.headers returns a dictionary with information about the url's HTML's header.
                 #
for x in r.headers:
    print("{} : {}".format(x,r.headers[x]))

url = "https://www.w3schools.com/PYTHON/ref_requests_response.asp"

print(requests.get(url).elapsed)
print(requests.get(url).history)
print(requests.get(url).links)
print(requests.get(url).ok)
print(requests.get(url).iter_lines())
print(requests.get(url).reason)
print(requests.get(url).encoding)
print(requests.get(url).apparent_encoding)

url = str(requests.get(url))
soup = BeautifulSoup(url,'html.parser')
print(soup[900])

print(url.content)
# print(requests.get(url).text)
'''
img = matplotlib.image.imread('header')
imgplot = matplotlib.pyplot.imshow(img)
matplotlib.pyplot.show()

with open(r) as joe:
    soup = BeautifulSoup(joe)
soup = BeautifulSoup("<html>data</html>")
print(soup)
'''

# print(dir(r))
# print(help(r))

# print(r.text)
# print(r.encoding)



'''
Requests documentation following examples
'''

r = requests.get('https://api.github.com/events')
