import math

def checkPalindrome(word):

    if len(word) % 2 == 0: # even amount of letters

        for i in word: # i is element of index i in word

            if (word[word.index(i)].lower() == word[(len(word) - 1) - word.index(i)].lower()): # check if 'mirrored letter' is identical

                continue # if letter is identical continue with for loop

            else:

                return False # if letter is not identical return False

        return True # if all letters were checked the for loop ends and False is returned

    else:

        for i in word: # uneven amount of letters

            if (word[word.index(i)].lower() == word[(len(word) - 1) - word.index(i)].lower()): # check if 'mirrored letter' is identical to letter

                continue # if letter is identical to 'mirrored letter', continue with for loop

            else:

                return False # if letter is not identical to 'mirrored letter' return False

        return True # if all letters are compared to their corresponding 'M.L' the for loop ends and True is returned

def convertToString(word):

        str = ''

        for i in word.split():

            str += i

        return checkPalindrome((str))

while True:

    print(convertToString(str(input("Give a word or a sentence. I'll check if it's a palindrome or not : "))))






