import math

class player():

    def __init__(self,playerName,move):

        self.name = playerName
        self.move = move

    def playersMove(self):

        return "{} PLAYED {}".format(self.name.upper(),self.move.upper())

    def giveMove(self):

        return self.move[0].lower()

    def giveName(self):

        return self.name.upper()

class game():

    def __init__(self,player1,player2):

        self.player1 = player1
        self.player2 = player2

    def comparePlays(self):

        plays = ["r","p","s"]
        play1 = self.player1.giveMove()
        play2 = self.player2.giveMove()

        for i in range(len(plays)):

            if plays[i] == play1 and plays[i] == play2:

                return "The game between {} and {} ended in a draw".format(player1.giveName(),player2.giveName())

            elif (plays[i] == play1 or plays[i] == play2) and (plays[i+1] == play1 or plays[i + 1] == play2):

                if plays[i] == play1:

                    return "{} WINS !!!!!".format(player1.giveName())

                else:

                    return "{} WINS !!!!!".format(player2.giveName())

            else: 

                if plays[i] == play1:

                    return "{} WINS !!!!!".format(player1.giveName())

                else:

                    return "{} WINS !!!!!".format(player2.giveName())




player1 = player("Casper","Rock") # werkt
print(player1.playersMove())
player2 = player("Yannick","Scissors") # werkt
print(player2.playersMove())

game1 = game(player1,player2) # werkt
print(game1.comparePlays()) 


