import random, string

def passwordGenerator(strength,hashing):

    characters = string.printable
    password = ''

    for i in range(6*strength):

        password += characters[random.randint(0,len(characters)- 1)]

    if hashing == True:
        
        return hash(password)

    else:

        return password 


while True:

    strength = int(input("How strong do you want your pasword to be between 1 and 10? : "))
    hashing = False 
    print(passwordGenerator(strength,hashing))

        


