from make_list import listNumbers

def remove_duplicates(alist):

    list = []

    for i in range(len(alist)):

        if alist.index(alist[i]) != i:

            continue 

        else:

            list.append(alist[i])

    return list 
         

list = [1,2,3,4,3,2,1,5,4]

print(remove_duplicates(list))

print(remove_duplicates(listNumbers()))

'''
list.index(<something>) returns the index of the first occurence of <something>.
Therefore, if you are checking for the index of the first occurence of <something> you see that you've encountered a duplicate when the index of the first occurence doesn't match the index of the current item's index. 
'''


