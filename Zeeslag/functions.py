import random
import math

height = 500
width = 500

''' Making random x and y cooridinates on a grid with the specified dimensions'''
print()
X = random.randint(0,(width - 1))
print("{} is {} ".format("X",X))
Y = random.randint(0,(height - 1))
print("{} is {} ".format("Y", Y))

''''User gets to ask questions. Here's a list of the formats of the questions'''

questions = ["Within radiux Z widh (X,Y) as center?","Are you at (X,Y)","Are you North/South of Y? ","Are you East/West of X? "]

''' Function that is used to choose a question from the list of questions '''

def pickQuestion():
    print()
    print("Select number sort of question")

    a = 0

    for i in range(len(questions)):
        print("{}. {}".format(a, questions[i]))
        a += 1

    je_index = int(input("Give the integer that corresponds with the question you'd like to ask : "))
    print()

    askQuestion(je_index) 

''' Function that is used to specify question variables that appear in the question'''

def askQuestion(questionIndex):

    '''check(questionIndex,x,y,z,northSouth,eastWest)'''

    if questionIndex == 0:

        x = int(input("What is the x coördinate of your circle? : "))
        y = int(input("What is the y coördinate of your circle? : "))
        z = float(input("What is the radius of your circle"))
        northSouth = None
        eastWest = None
        print()

        check(questionIndex,x,y,z,northSouth,eastWest )

    elif questionIndex == 1:

        x = int(input("What is the x coördinate? : "))
        y = int(input("What is the y coördinate? : "))
        z = None
        northSouth = None
        eastWest = None
        print()

        check(questionIndex,x,y,z,northSouth,eastWest)

    elif questionIndex == 2:
        
        x = None 
        y = int(input("What is the y coördinate? : "))
        z = None 
        northSouth = input("Check if the ones to be found are North or South of the specified y coordinate (north or south) : ")
        eastWest = None
        print()

        check(questionIndex,x,y,z,northSouth,eastWest)

    else:

        x = int(input("What is the x coördinate? : "))
        y = None
        z = None 
        northSouth = None 
        eastWest = input("Check if the ones to be found are East or West of the specified y coordinate? : ")
        print()

        check(questionIndex,x,y,z,northSouth,eastWest)

def check(questionIndex,x,y,z,northSouth,eastWest):

    if questionIndex == 0:

        distance = lambda x, y, X, Y: math.sqrt(math.pow(abs(x - X), 2) + math.pow(abs(y - Y), 2))

        if distance(x,y,X,Y) - z >= 0:

            print("nee")

        else:

            print("ja")

    elif questionIndex == 1:

        if (x - X) == 0  and (y - Y) == 0:

            print("kut, je hebt me gevonden. proficiat, je hebt gewonnen")
            print("druk op Ctrl+c en druk dan op het pijltje omhoog en dan enter om nog eens te spelen")
            

        else:

            print("nee")

    elif questionIndex == 2:

        if northSouth[0].lower() == "n" and Y >= y or northSouth[0].lower() == "s" and Y <= y:

            print("ja") 

        else:

            print("nee") 

    else:

        if eastWest[0].lower() == "w" and X <= y or eastWest[0].lower() == "e" and Y >= y:

            print("ja")

        else:

            print("nee")

while True:

    pickQuestion()

