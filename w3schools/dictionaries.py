carDict = {
        "model" : "xp",
        "weight" : 1000, 
        "color" : "red"
        }

print(carDict["model"])
print(carDict.get("color"))
print()

for i in carDict:
    print(i) # print alle sleutels

for i in carDict.values():
    print(i)
for a,b in carDict.items():
    print(str(a)+" : "+str(b))

print() # de 1e for-loop voor en na deze regel doen hetzelfde

for x,y in carDict.items():
    print("{} : {}".format(x,y))

carDict["ziekte"] = "kanker" # komt achteraan in woordenboek
print(carDict)

carDict.pop("ziekte") # verwijdert sleutel en waarde uit woordenboek
print(carDict)

carDict["chassis material"] = "duraluminium"
print(carDict)
carDict.popitem()
print(carDict)
print("Printing dict 2")
dict2 = carDict.copy()
print(dict2)
print()
print("printing dict3") 
dict3 = dict(carDict)
print(dict3)
print()



# using 'dict()' as a constructor

dict = dict(naam = "Casper", leeftijd = 21)
print(dict) 









