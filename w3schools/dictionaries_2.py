'''Initializing a dictonary'''

dict = {
        "name" : "Casper Haan",
        "age" : 21,
        }

print(dict) 

'''Two ways retain the value of a key from a dictionary'''

print(dict.get("name"))
print(dict["name"])

'''Altering a value of a key in dictionary'''

dict["name"] = "Ruben Haan"
print(dict.get("name"))

'''Printing all keys values and all keys of a dictionary'''

for i in dict:
    print("{} value has the key : {}".format(i,dict[i]))

''' Printing all the values of a dictionary''' 

for x in dict.values():
    print(x)

''' Again printing all the keys and their respective values''' 

for x,y in dict.items():
    print(x,y)

''' Adding a new key-value combination to the dictionary '''

dict["heeftKanker"] = False
print(dict)

'''Removing a key-value combination'''

dict.pop("age")
print(dict)

''' Copy dictionary '''

dict2 = dict.copy()
print(dict,dict2) # prints both dictionaries on same line of CLI

''' Nested dictionaries '''

family = {
        "mother": {
            "name ": "Anja",
            "age " : 56
            },
        "father": {
            "name" : "Ronald",
            "Age " : 52
            },
        "brother" : {
            "name" : "Ruben",
            "age ": 18
            }
        }

for i in family: # prints names of nested dictionaries
    print(i)


