class person():
	def __init__(self,firstName,lastName):
		self.firstName = firstName
		self.lastName = lastName
	def giveFirstName(self):
		return self.firstName
	def giveLastName(self):
		return self.lastName
	def giveFullName(self):
		return "My name is {} {}".format(self.firstName,self.lastName)

class president(person):
	
	def __init__(self,firstName,lastName,country):
		super().__init__(firstName,lastName)
		self.country = country
	def giveCountry(self):
		return "{} {} is president of {}".format(self.firstName,self.lastName,self.country)

# making a person object named donaldTrump
donaldTrump = person("Donald","Trump")
print(donaldTrump.giveFullName())

#Making a president object named Donald_J_Trump
Donald_J_Trump = president("Donald","Trump","USA")
print(Donald_J_Trump.giveCountry())
print(Donald_J_Trump.giveFullName())
