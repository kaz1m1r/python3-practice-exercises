import random,string

populationSize = 1000
targetString = 'nibber'
amountOfGenes = len(targetString)
mutationRate = 1 # integer percentage
myPopulation = []

# FUNCTION THAT GENERATES AN INDIVIDUAL WITH THE AMOUNT OF GENES OF THE INDIVIDUAL AS PARAMETER
def randomIndividual(amountOfGenes):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(amountOfGenes))

# FUNCTION THAT MAKES A POPULATION WITH THE POPULATIONSIZE AS ITS PARAMETER
def makePopulation(populationSize,amountOfGenes):
    population = []
    for i in range(populationSize):
        population.append(randomIndividual(amountOfGenes))

    myPopulation = population.copy()
    fitnessFunction(population,targetString)

# PUTS THE RESPECTIVE FITNESSES OF EVERY INDIVIDUAL IN A LIST
def fitnessFunction(population,targetString):
    fitnessList = []

    for i in population: # ITERATES THROUGH INDIVIDUALS IN POPULATION
        fitnessPoints = 0
        if i == targetString: # TARGETSTRING FOUND IN CURRENT POPULATION
            print("TARGET FOUND !!!")
            break
        else:
            for j in range(len(targetString)): # ITERATES THROUGH ALL GENES OF INDIVIDUAL
                if i[j] == targetString[j]: # INDIVIDUAL HAS SAME LETTER AS TARGETSTRING IN SAME INDEX
                    fitnessPoints += 1
            fitnessList.append(fitnessPoints) # AFTER CHECKING EVERY GENE OF INDIVIDUAL, FITNESSSCORE IS PUT INTO FITNESSLIST
    fittestHalf(fitnessList)

# CALCULATES THE AVERAGE FITNESS OF THE CURRENT POPULATION
def averageFitness(fitnessList):
    totalFitness = 0
    for i in fitnessList:
        totalFitness += i
    return totalFitness/len(fitnessList)

# MAKES LIST OF THE TOP 50 PERCENT HIGHEST FITNESSES OF THE POPULATION
def fittestHalf(fitnessList):
    sortedFitness = sorted(fitnessList, reverse = True) # SORTS FITNESSLIST FROM HIGH TO LOW
    fittestHalf = []
    for i in sortedFitness:
        if sortedFitness.index(i) == (len(sortedFitness)/2):
            fittestIndividuals(fittestHalf,myPopulation,targetString)
        else:
            fittestHalf.append(i)

# MAKES A LIST OF TOP 50 PERCENT INDIVIDUALS OF THE POPULATION
def fittestIndividuals(fittestHalf,population,targetString):
    fittestIndividuals = []
    for i in population:
        fitnessPoints = 0
        for j in range(len(targetString)):
            if i[j] == targetString[j]:
                fitnessPoints += 1
        if fittnesPoints in fittestHalf and len(fittestIndividuals) != len(fittestHalf):
            fittestIndividuals.append(i)
        else:
            continue
    crossOver(fittestIndividuals,targetString,mutationRate)

# USES THE LIST WITH FITTEST INDIVIDUALS TO GENERATE A NEW LIST THAT REPRESENTS THE NEXT GENERATION OF THE POPULATION
def crossOver(fittestIndividuals,targetString,mutationRate):
    originalLength = len(fittestIndividuals) - 1
    letters = string.ascii_lowercase
    while len(fittestIndividuals) < 1000:
        parent1 = random.randint(0,originalLength)
        parent2 = random.randint(0,originalLength)
        kid = ''
        for i in range(len(targetString)):
            geneChoice = random.randint(0,1)
            chance = random.randint(1,100) # MUTATION OF A RANDOM GENE IN THE KID IS ALSO COVERED IN THE FOR LOOP
            if geneChoice == 0:
                if chance <= mutationRate:
                    kid.join(random.choice(letters))
                else:
                    kid.join(fittestIndividuals[parent1][i])
            else:
                if chance <= mutationRate:
                    kid.join(random.choice(letters))
                else:
                    kid.join(fittestIndividuals[parent2][i])
        fittestIndividuals.append(kid)
    fitnessFunction(fittestIndividuals,targetString)

# RUN THIS FUNCTION TO START YOUR GENETIC ALGORITHM
def startGA():
    makePopulation(populationSize,amountOfGenes,targetString)



print(makePopulation(20,10))
print(startGA)































