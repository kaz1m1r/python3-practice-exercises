from hangman_figures import hangman_figures

def check(word):

    correctlyGuessed = []
    wronglyGuessed = ""
    mistakes = 0


    for i in range(len(word)):
        correctlyGuessed.append("_")

    # correctly guessed
    while True:

        print()

        guess = input("GUESS A LETTER : ").lower()

        if guess == "exit":

            print("BYE")
            break

        if guess in word:

            print("You guessed correctly")

            for i in range(len(word)):

                if word[i] == guess:

                    correctlyGuessed[i] = guess.upper()

                else:

                    continue

        else:

            print("You guessed incorrectly")

            wronglyGuessed += guess.upper()
            mistakes+=1

        hangman_figures(mistakes)
        print()
        print("CORRECTLY GUESSED : {} \nWRONGLY GUESSED : {}".format("".join(correctlyGuessed),wronglyGuessed))




