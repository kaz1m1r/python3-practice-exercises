# python3 practice exercises

This 'Project' is a description of my progress in python3. The master-branch is insignificant since this isn't a project that I'll be releasing. Every branch of master is named after the source for the python3 exercises that I make.