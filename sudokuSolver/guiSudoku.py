from tkinter import Tk , StringVar 
from tkinter.ttk import Frame,Entry,Button 
import numpy as np
from solving_sudoku import check, solve 
import time

value_sudoku_matrix = [[],[],[],[],[],[],[],[],[]]
solved = False
solve_button_pressed = False 

'''Class for 9x9 sudoku grid Frame widget '''
class Sudoku_grid(Frame):
    def __init__(self,sudoku_solver_window,border_distance):
        super().__init__()
        self.sudoku_solver_window = sudoku_solver_window 
        self.border_distance = border_distance 
        self.sudoku_matrix = [[],[],[],[],[],[],[],[],[]]
        self.fill_with_entries()
        self.add_solve_button()
    
    while solve_button_pressed:
        self.make_sudoku_integer_matrix()
    
    while solved:
        self.display_solved_sudoku()
                
    def make_sudoku_integer_matrix(self):
        for y in range(9):
            for x in range(9):
                value_sudoku_matrix[y].append(self.sudoku_matrix[y][x].give_entry_value())
    
    def display_solved_sudoku(self):
        for y in range(9):
            for x in range(9):
                self.sudoku_matrix[y][x].set(str(value_sudoku_matrix[y][x]))
    
    def fill_with_entries(self):
        # putting entry objects in a sudoku_matrix (nested lists)
        for row in range(9):
            for entries in range(9):
                self.sudoku_matrix[row].append(Sudoku_entry(self.sudoku_solver_window))
        # placing entries on Sudoku_grid instance 
        self.starty_entry = 30
        self.startx_entry = 30
        self.height_entry = 80
        self.width_entry = 80
        for y in range(9):
            self.startx_entry = 30
            if y != 0:
                self.starty_entry += self.height_entry 
            for x in range(9):
                self.sudoku_matrix[y][x].place(x = self.startx_entry, y = self.starty_entry, width = self.width_entry, height = self.height_entry)
                self.startx_entry += self.width_entry 
                
    def add_solve_button(self):
        self.starty_button = 30 + (9 * self.height_entry) + 30
        self.startx_button = 30 + (9 * self.width_entry) + 30
        Sudoku_solve_button(self.sudoku_solver_window).place(x = self.startx_button, y = self.starty_button)
                
    '''Entry widget class for 9x9 sudoku'''
class Sudoku_entry(Entry):
    def __init__(self,sudoku_solver_window):
        super().__init__()
        self["font"] = ("Verdana",40)
        self["justify"] = "center"
        self.value = StringVar()
        self["textvariable"] = self.value 
        
    def give_entry_value(self):
        if self.value.get() == '':
            return 0
        elif self.value.get() in '123456789':
            return int(self.value.get())
        else:
            return None 
    
    def set_entry_value(self,value_to_set):
        self.value.set(value_to_set)

def solve_sudoku():
    global solve_button_pressed
    solve_button_pressed = True
    # alter value_sudoku_matrix to corresponding matrix of solved sudoku 
    time.sleep(0.5)
    display_solved_sudoku_matrix()
    
def display_solved_sudoku_matrix():
    global solve_button_pressed
    global solved 
    if solve_button_pressed == True:
        solved_sudoku = solve(value_sudoku_matrix)
        solved = True 
        value_sudoku_matrix = solved_sudoku 
        
class Sudoku_solve_button(Button):
    def __init__(self,to_solve_sudoku_grid):
        super().__init__()
        self.grid = to_solve_sudoku_grid
        self['text'] = "Solve"
        self["command"] = solve_sudoku()
    
if __name__ == "__main__":
    sudoku_solver = Tk()
    sudoku_solver.title("Sudoku solver --- by : Casper Haan")
    sudoku_grid_padding = 8
    sudoku_grid = Sudoku_grid(sudoku_solver, sudoku_grid_padding)
    sudoku_solver.mainloop()