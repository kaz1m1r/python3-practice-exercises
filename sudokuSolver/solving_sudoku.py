import numpy as np
import time
'''
Source:
https://sudoku.com/expert/
'''
sudoku = [[6,0,9,1,0,2,0,8,0],
          [0,0,0,0,0,0,4,0,0],
          [5,0,2,0,0,0,0,0,0],
          [0,0,0,0,2,0,3,0,4],
          [1,0,0,0,0,5,0,0,0],
          [0,2,0,0,0,0,5,0,6],
          [0,0,0,8,0,1,0,0,0],
          [0,0,0,0,0,0,0,0,9],
          [8,0,5,9,0,7,0,4,0]]
'''
making a copy of the initial grid
'''
sudoku_copy = sudoku.copy() 
def check(sudoku,xco,yco,num):
    '''
    If num is found in the same row/column as the given x/y coordinate return False
    '''
    for x in range(9):
        if sudoku[x][yco] == num:
            return False
    for y in range(9):
        if sudoku[xco][y] == num:
            return False 
    '''
    x and y coordinates of top left celf of 1 of the 9 squares in the sudoku is calculated.
    '''
    x1 = (xco // 3) * 3 # x1 becomes either 0, 3 or 6
    y1 = (yco // 3) * 3 # y1 becomes either 0, 3 or 6
    for x in range(x1, (x1 + 3)):
        for y in range(y1, (y1 + 3)):
            if sudoku[x][y] == num:
                return False
    return True 
'''
Making a variable that saves the current time since 1/1/1970 in seconds. 
It's basically a reference to the current time. 
'''
def solve(sudoku):
    for x in range(9):
        for y in range(9):
            if sudoku[x][y] == 0:
                for n in range(1,10):
                    if check(sudoku,x,y,n):
                        sudoku[x][y] = n
                        solve(sudoku)
                        sudoku[x][y] = 0
                    else:
                        continue
                return
    return sudoku 
'''
Een return stopt de oproeping van de functie. Als functie check alle vakken heeft 
gevuld stopt de binnenste for loop. return wordt dan opgeroepen. Dit stopt de oproeping 
van alle functies opgeroepen in de recursieve oproepingen. De ingevulde sudoku wordt 
vervolgens geprint en het programma eindigd. 

Daarnaast, als een vak niet gevuld kan worden met de huidige voorgestelde waarde. 
Als geen enkel getal ingevuld kan worden, dan wordt de huidige oproeping van de functie 
opgeheven door de return. Dan ga je weer terug naar je vorige waarde van n en kijk je of 
je nog een ander getal in kan vullen. Als dit nieuwe getal bij je vorige n ervoor zorgt 
dat je nu ook een nieuwe n in kan vullen, dan ga je door met het invullen van een waarde 
n in de volgende lege vakken. 

Als n ingevuld kan worden dan wordt de functie nogmaals opgeroepen op een recursieve manier. 
ik vul een n in. De sudoku verandert. Met deze sudoku wordt de functie nogmaals opgeroepen. 
De functie zoekt een vak met een 0 en probeert een waarde in te vullen. 
'''
            
if __name__ == '__main__':

    solve(sudoku)
