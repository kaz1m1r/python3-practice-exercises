import curses,time 
'''
stdscr = curses.initscr()
When you press a key when you're in terminal, this key gets displayed in the terminal window.
curses.noecho() makes sure that what you type in terminal isn't displayed in the terminal. It just serves as an input for the python program
curses.noecho()
curses.cbreak() ensures your input will serve as an argument for your python program without the user having to press ENTER. 
curses.cbreak()
stdscr.keypad(True)
fill in respectively, x y and text in .addstr() method to display text on terminal instance.
stdscr.addstr(10,15,"JoeJoe") 
refresh the instance of the screen after adding the text 'JoeJoe' to it. .refresh() basically applies all the changes that you made to the initial instance of the screen.  
stdscr.refresh() 
Pause the program for 10 seconds
time.sleep(10)
curses.echo()
curses.nocbreak()
stdscr.keypad(False) 
curses.endwin() ends the instance of the window 
curses.endwin()
'''
# -----------------------------------

def main(stdscr):
    curses.curs_set(0)
    curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_YELLOW)

    text = "JoeJoe"
    texLen = len(text)
    texLen = len(text)
    h,w = stdscr.getmaxyx()

    height = lambda y,t : y//2 - t 
    width = lambda x : x//2

    stdscr.attron(curses.color_pair(1))
    stdscr.addstr(height(h,texLen),width(w),text)
    stdscr.attroff(curses.color_pair(1))

    stdscr.refresh()
    time.sleep(4)

if __name__ == "__main__":
    curses.wrapper(main)
