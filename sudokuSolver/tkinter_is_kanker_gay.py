from tkinter import Tk, StringVar 
from tkinter.ttk import Button, Entry 

root = Tk()
dimensions = "300x300"
root.geometry(dimensions)

text_var_1 = StringVar()
text_var_2 = StringVar()
entry_1 = Entry(root,justify = 'center',textvariable = text_var_1).grid(row = 0, column = 0)

def print_entry_value():
    if text_var_1.get() == '':
        # Empyt string is in the entry object by default 
        print("Empty string is retreived with the .get() method")
    else:
        hoi = 4 * int(text_var_1.get())
        print("Entry value times four is {}".format(hoi))

button_1 = Button(root, text= 'Print_value_entry',command = print_entry_value).grid(row = 0, column = 1)

root.mainloop()

