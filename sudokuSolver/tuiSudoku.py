import curses,sys,os,string,time 
import numpy as np 
from solving_sudoku import check 

# Sudoku lines
sudokuZeroesLine1 = "000000000"
sudokuZeroesLine2 = "000000000"
sudokuZeroesLine3 = "000000000"
sudokuZeroesLine4 = "000000000"
sudokuZeroesLine5 = "000000000"
sudokuZeroesLine6 = "000000000"
sudokuZeroesLine7 = "000000000"
sudokuZeroesLine8 = "000000000"
sudokuZeroesLine9 = "000000000"

def stringchanger(string,index,number):
    string = string[:index] + str(number) + string[index + 1:]

def solve(sudoku):

    global sudokuZeroesLine1
    global sudokuZeroesLine2
    global sudokuZeroesLine3
    global sudokuZeroesLine4
    global sudokuZeroesLine5
    global sudokuZeroesLine6
    global sudokuZeroesLine7
    global sudokuZeroesLine8
    global sudokuZeroesLine9 

    for y in range(9):
        for x in range(9):
            if sudoku[y][x] == 0:
                for n in range(1,10):
                    if check(sudoku,y,x,n):
                        sudoku[y][x] = n
                        solve(sudoku)

                        # backtracking if we encounter a fuck up

                        sudoku[y][x] = 0
                    else:
                        continue
                return

    # index of row in sudoku matrix. This for loop starts when sudoku is filled with non-zero numbers

    for row in range(len(sudoku)):

        # index of current item in current row of sudoku matrix 

        for num in range(len(sudoku[row])):

            if row == 0:
                sudokuZeroesLine1 = sudokuZeroesLine1[:num] + str(sudoku[row][num]) + sudokuZeroesLine1[num + 1:] 
            elif row == 1:
                sudokuZeroesLine2 = sudokuZeroesLine2[:num] + str(sudoku[row][num]) + sudokuZeroesLine2[num + 1:] 
            elif row == 2:
                sudokuZeroesLine3 = sudokuZeroesLine3[:num] + str(sudoku[row][num]) + sudokuZeroesLine3[num + 1:] 
            elif row == 3:
                sudokuZeroesLine4 = sudokuZeroesLine4[:num] + str(sudoku[row][num]) + sudokuZeroesLine4[num + 1:] 
            elif row == 4:
                sudokuZeroesLine5 = sudokuZeroesLine5[:num] + str(sudoku[row][num]) + sudokuZeroesLine5[num + 1:] 
            elif row == 5:
                sudokuZeroesLine6 = sudokuZeroesLine6[:num] + str(sudoku[row][num]) + sudokuZeroesLine6[num + 1:] 
            elif row == 6:
                sudokuZeroesLine7 = sudokuZeroesLine7[:num] + str(sudoku[row][num]) + sudokuZeroesLine7[num + 1:] 
            elif row == 7:
                sudokuZeroesLine8 = sudokuZeroesLine8[:num] + str(sudoku[row][num]) + sudokuZeroesLine8[num + 1:] 
            elif row == 8:
                sudokuZeroesLine9 = sudokuZeroesLine9[:num] + str(sudoku[row][num]) + sudokuZeroesLine9[num + 1:] 
            else:
                pass 

def tui(stdscr):

    global sudokuZeroesLine1
    global sudokuZeroesLine2
    global sudokuZeroesLine3
    global sudokuZeroesLine4
    global sudokuZeroesLine5
    global sudokuZeroesLine6
    global sudokuZeroesLine7
    global sudokuZeroesLine8
    global sudokuZeroesLine9 

    # Initial value of key
    k = 0

    # Initial coördinates (pick top left of empty sudoku) 
    cursor_x = 0
    cursor_y = 0
    stdscr.clear()
    stdscr.refresh() 

    # start colors and initialize a couple color pairs (FG then BG) 
    curses.start_color()
    curses.init_pair(1 , curses.COLOR_RED , curses.COLOR_BLACK)
    curses.init_pair(2 , curses.COLOR_BLACK , curses.COLOR_WHITE)
    curses.init_pair(3 , curses.COLOR_WHITE , curses.COLOR_BLACK)


    while k != ord('q'):

        #initialization
        stdscr.clear()
        height, width = stdscr.getmaxyx()

        # getting current x and y of cursor
        cursor_x = max(0,cursor_x)
        cursor_x = min(width-1,cursor_x)
        cursor_y = max(0,cursor_y)
        cursor_y = min(height-1,cursor_y) 

        # All text
        title = "SUDOKU SOLVER"[:(width - 1)]
        subtitle = "Change zeroes to other numbers between 1-9 as a way of inserting numbers. Press arrow key after pressing 's' to display solution."[:(width - 1)]
        statusBar = "Press 'q' to exit | Press 's' to solve | STATUS BAR | x = {}, y = {}".format(cursor_x , cursor_y)
        
        # centering text
        start_x_title = int((width // 2) - (len(title) // 2) - len(title) % 2)
        start_x_subtitle = int((width // 2) - (len(subtitle) // 2) - (len(subtitle) % 2))

        start_x_sudoku = int((width // 2 ) - (len(sudokuZeroesLine1) // 2) - (len(sudokuZeroesLine1) % 2))
        start_y = int(height // 4)

        # making status bar
        stdscr.attron(curses.color_pair(2))
        stdscr.addstr(height-1,0,statusBar)
        stdscr.addstr(height-1, len(statusBar) , " " * (width - len(statusBar) - 1))
        stdscr.attroff(curses.color_pair(2))

        # ATTACHING ALL TEXT TO SCREEN
        # title
        stdscr.attron(curses.color_pair(1))
        stdscr.attron(curses.A_BOLD)
        stdscr.addstr(start_y - 1, start_x_title, title)
        
        # removing title attributes
        stdscr.attroff(curses.color_pair(1))
        stdscr.attroff(curses.A_BOLD)

        # subtitle
        stdscr.addstr(start_y + 1, start_x_subtitle, subtitle)

        # adding sudoku to screen
        stdscr.addstr(start_y + 3, start_x_sudoku , sudokuZeroesLine1) 
        stdscr.addstr(start_y + 4, start_x_sudoku , sudokuZeroesLine2) 
        stdscr.addstr(start_y + 5, start_x_sudoku , sudokuZeroesLine3) 
        stdscr.addstr(start_y + 6, start_x_sudoku , sudokuZeroesLine4) 
        stdscr.addstr(start_y + 7, start_x_sudoku , sudokuZeroesLine5) 
        stdscr.addstr(start_y + 8, start_x_sudoku , sudokuZeroesLine6) 
        stdscr.addstr(start_y + 9, start_x_sudoku , sudokuZeroesLine7) 
        stdscr.addstr(start_y + 10, start_x_sudoku , sudokuZeroesLine8) 
        stdscr.addstr(start_y + 11, start_x_sudoku , sudokuZeroesLine9) 

        # k = stdscr.getch() 
        if k == curses.KEY_DOWN or k == ord('j'):
            cursor_y += 1
        elif k == curses.KEY_UP or k == ord('k'):
            cursor_y -= 1
        elif k == curses.KEY_LEFT or k == ord('l'):
            cursor_x -= 1
        elif k == curses.KEY_RIGHT or k == ord('h'):
            cursor_x += 1 

        # pressed 's' so the sudoku should be solved 
        elif k == ord('s'):

            sudoku = [
                    list(sudokuZeroesLine1),
                    list(sudokuZeroesLine2),
                    list(sudokuZeroesLine3),
                    list(sudokuZeroesLine4),
                    list(sudokuZeroesLine5),
                    list(sudokuZeroesLine6),
                    list(sudokuZeroesLine7),
                    list(sudokuZeroesLine8),
                    list(sudokuZeroesLine9)
                    ]

            for row in range(len(sudoku)):
                for num in range(len(sudoku[row])):
                    a = int(sudoku[row][num])
                    sudoku[row].insert(num,a)
                    sudoku[row].pop(num + 1)

            solve(sudoku) 


        else:

            # k equals ascii value of any number between 1 up until 9

            if k == 48 or k == 49 or k == 50 or k == 51 or k == 52 or k == 53 or k == 54 or k == 55 or k == 56 or k == 57:
                for y in range(start_y + 3, start_y + 12):
                    if y == cursor_y:
                        for x in range(start_x_sudoku, start_x_sudoku + len(sudokuZeroesLine1)):
                            if x == cursor_x:

                                # when you get here it's confirmed that cursor is placed on drawn sudoku 'grid'

                                index = cursor_x - start_x_sudoku
                                replacement = str(chr(k))

                                if cursor_y == start_y + 3:
                                    sudokuZeroesLine1 = sudokuZeroesLine1[:index] + replacement + sudokuZeroesLine1[index + 1:]
                                elif cursor_y == start_y + 4:
                                    sudokuZeroesLine2 = sudokuZeroesLine2[:index] + replacement + sudokuZeroesLine2[index + 1:]
                                elif cursor_y == start_y + 5:
                                    sudokuZeroesLine3 = sudokuZeroesLine3[:index] + replacement + sudokuZeroesLine3[index + 1:]
                                elif cursor_y == start_y + 6:
                                    sudokuZeroesLine4 = sudokuZeroesLine4[:index] + replacement + sudokuZeroesLine4[index + 1:]
                                elif cursor_y == start_y + 7:
                                    sudokuZeroesLine5 = sudokuZeroesLine5[:index] + replacement + sudokuZeroesLine5[index + 1:]
                                elif cursor_y == start_y + 8:
                                    sudokuZeroesLine6 = sudokuZeroesLine6[:index] + replacement + sudokuZeroesLine6[index + 1:]
                                elif cursor_y == start_y + 9:
                                    sudokuZeroesLine7 = sudokuZeroesLine7[:index] + replacement + sudokuZeroesLine7[index + 1:]
                                elif cursor_y == start_y + 10:
                                    sudokuZeroesLine8 = sudokuZeroesLine8[:index] + replacement + sudokuZeroesLine8[index + 1:]
                                elif cursor_y == start_y + 11:
                                    sudokuZeroesLine9 = sudokuZeroesLine9[:index] + replacement + sudokuZeroesLine9[index + 1:]
                                else:
                                    pass 
                            else:
                                continue
                    else:
                        continue 
            else:
                pass 

        # making sure that the current cursorposition is rendered 
        stdscr.move(cursor_y , cursor_x)
        
        # refresh screen
        stdscr.refresh()

        # Input user, getch() returns an integer value between 0 and 255
        k = stdscr.getch()

def main():
    curses.wrapper(tui)
    print(sudokuZeroesLine1)
    print(sudokuZeroesLine2)
    print(sudokuZeroesLine3)

if __name__ == "__main__":
    main()

