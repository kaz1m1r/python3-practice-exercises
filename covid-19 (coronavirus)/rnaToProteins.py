from covid_RNA import *

''' 
-----------
BIOLOGY 101
-----------

Codon AUG begint de translatie van RNA naar DNA.
Codon UAA stopt de  translatie todat weer een ATG codon gedetecteerd wordt.

Als het RNA wordt gegeven op een manier waarop T niet gesubstitueerd is door U,
volg dan de volgende regels.

Codon ATG begint de translatei van RNA naar DNA
Codon TAA stopt de translatie todat weer een ATG codon gevonden wordt.


'''

# https://stackoverflow.com/questions/19521905/translation-dna-to-protein

translatie = {
    'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
    'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
    'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
    'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
    'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
    'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
    'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
    'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
    'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
    'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
    'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
    'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
    'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
    'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
    'TAC':'Y', 'TAT':'Y', 'TAA':'_', 'TAG':'_',
    'TGC':'C', 'TGT':'C', 'TGA':'_', 'TGG':'W',
    }

startcodon = "ATG"
print(startcodon[1])
stopcodons = ["TAA","TAG","TGA"]

''' TE DOEN

######################################

AL DIE KUT INDENTATIE FOUTEN WEG HALEN
* In pycharm, als je een doffe geelbruine kleur links van je code ziet
  moet je dit verbeteren.
* Deze verbetering doe je het snelst door de code overnieuw te schrijven.
* Je kan het beste het hele bestand overnieuw schrijven. Of een script opzoeken
  wat deze indentatie fouten verwijderd

#######################################
'''

proteins = {}
proteinindex = 0
proteinName = "Protein "+str(proteinindex)
aAcids = ""
rnb = giveCoronaRNA()
rna = rnb


def rnaToAmino(rna):
	'''Converts the rna as a string to amino acids as a string '''
	aa = ''
	index = 2
	codon = rna[index - 2] + rna[index - 1] + rna[index]
	while index != len(rna):
		
		if codon in translatie:
			aa += translatie[codon]
			index += 3
		else:
			print("you discovered a new protein")
	aAcids = aa
rnaToAmino(rna)
print(aAcids)
def findStart(aaString):
    ''''''
'''Returns stringed amino acids beginning with the M   '''

    start = aaString.find("M")
    return aaString[start:]

def findStop(aaString):
	'''Returns the index of the protein's string's first _ '''
	return aaString.find("_")

def proteinList(aaString,start,stop):
	'''Puts the part of aaString between a M and a _ in a dictionary
	AKA this function puts proteins and their respectively corresponding names in a dictionary
	'''
        global aAcids 

	proteins[proteinName] = aaString[start:stop]
        aAcids2 = aAcids[stop:]
        aAcids = aAcids2 
	proteinindex += 1
    


rna = "attgattataaacactacacaccctacttttaagaaa"
print(rna.find("att")) # geeft de index van het begin van de gespecificeerde string



